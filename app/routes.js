module.exports = function(app, passport) {

    // Schemas:
    var Team   = require('./models/team');
    var Player = require('./models/player');
    var User = require('./models/user');

    app.route('/')
        .get(function(req, res) {
            if (req.user) {
                res.redirect('/content');
            } else {
                res.render('login.hbs', {
                    title: "Aktivitetsstøtte",
                    message: req.flash('loginMessage')
                });
            }
        })
        .post(passport.authenticate('local-login', {
            successRedirect : '/content', // redirect to the secure profile section
            failureRedirect : '/',
            failureFlash : true // allow flash messages
        }));

    app.route('/logout')
        .get(function(req, res) {
            req.logout();
            res.redirect('/');
        });

    app.route('/login')
        .get(function (req, res) {
            res.redirect('/');
        });

    app.route('/content')
        .get(isLoggedIn, function(req, res) {
            res.render('content.hbs', {
                user : req.user // get the user out of session and pass to template
            });
        });

    app.route('/createPlayer')
        .post(function (req, res) {
            var newPlayer = new Player({
                name: req.body.name,
                year: req.body.year,
                gender: req.body.gender
            });

            if(newPlayer) {
                console.log("newPlayer: " + newPlayer);
                newPlayer.save(function (err) {
                    if (err) {
                        res.status(500);
                        res.send(err);
                    } else {
                        res.status(200);
                        res.json(
                            newPlayer
                        );
                    }
                });
            }
        });

    app.route('/createTeam')
        .post(function (req, res) {
            var newTeam = new Team({
                name: req.body.name,
                trainers: req.body.trainers,
                players: [],
                activities: []
            });

            if(newTeam) {
                var isDone = false;
                console.log("newTeam: " + newTeam);
                newTeam.save(function (err) {
                    if (err) {
                        res.status(500);
                        res.send(err);
                    } else {
                        res.json(
                            newTeam
                        );
                    }
                });
            }
        });



    app.route('/createActivity')
        .post(function (req, res) {
            var teamId = req.body.teamId;
            var date = new Date(req.body.date);
            var day = date.getDate();
            if (day < 10) {
                day = "0" + day;
            }
            var month = (date.getMonth()+1);
            if (month < 10) {
                month = "0" + month;
            }
            var hr = req.body.startingTime.split(':')[0];
            if (hr < 10) {
                if (hr != "00") {
                    hr = "0" + hr;
                }
            }
            var min = req.body.startingTime.split(':')[1];
            if (min < 10) {
                if (min != "00") {
                    min = "0" + min;
                }
            }
            var newActivity = {
                name: req.body.name,
                date: day + "-" + month + "-" + date.getFullYear(),
                startingTime: hr + ":" + min,
                time: req.body.time,
                attendance: null
            };

            if(newActivity) {
                Team.findOne({"_id": teamId}, function (err, team) {
                    if (err) {
                        res.status(500);
                        res.send(err);
                    } else {
                        insertActivitySorted(res, newActivity, team);
                    }
                });
            }
        });

    app.route('/updateActivity')
        .post(function (req, res) {
            Team.findOne({"_id": req.body.teamId}, function (err, team) {
                for(var i = 0; i < team.activities.length; i++){
                    var date = team.activities[i].date.split('-');
                    if((date[0] + "-" + date[1] + "-" + date[2]) == req.body.searchParam1 &&
                        team.activities[i].startingTime == req.body.searchParam2.split('-')[0]){
                        if (req.body.date.length == 0){
                            //Update attendance
                            date = team.activities[i].date
                        } else {
                            //Update activity
                            date = new Date(req.body.date);
                            var day = date.getDate();
                            if (day < 10) {
                                day = "0" + day;
                            }
                            var month = (date.getMonth()+1);
                            if (month < 10) {
                                month = "0" + month;
                            }
                            date = day + "-" + month + "-" + date.getFullYear()
                        }
                        var hr = req.body.startingTime.split(':')[0];
                        if (hr < 10) {
                            if (hr != "00") {
                                hr = "0" + hr;
                            }
                        }

                        var min = req.body.startingTime.split(':')[1];
                        if (min < 10) {
                            if (min != "00") {
                                min = "0" + min;
                            }
                        }
                        team.activities.splice(i,1);
                        var newActivity = {
                            name: req.body.name,
                            date: date,
                            startingTime: hr + ":" + min,
                            time: req.body.time,
                            attendance: req.body.attendance
                        };
                        insertActivitySorted(res, newActivity, team);
                    }
                }
            });
        });

    app.route('/deleteActivity')
        .post(function (req, res) {
            Team.findOne({"_id": req.body.teamId}, function (err, team) {
                for (var i = 0; i < team.activities.length; i++) {
                    var date = team.activities[i].date.split('-');
                    var day = parseInt(date[0]);
                    if (day < 10) {
                        day = "0" + day;
                    }
                    if ((day + "-" + date[1] + "-" + date[2]) == req.body.searchParam1 &&
                        team.activities[i].startingTime == req.body.searchParam2.split('-')[0]) {
                        team.activities.splice(i, 1);
                        team.save();
                        res.status(200);
                        res.send();
                    }
                }
            })
        });

    app.route('/teams')
        .get(isLoggedIn, function (req, res) {
            Team.find({}, function (err, teams) {
                if(err){
                    console.log('ERROR: get /teams');
                    res.send(err);
                } else {
                    res.json(teams);
                }
            });
        });

    app.route('/updateTeamPlayers')
        .post(function (req, res) {
            Team.findOne({"_id": req.body.teamId}, function (err, team) {
                team.players = req.body.players;
                team.name = req.body.teamName;
                team.save();
                res.status(200);
                res.send();
            })
        });

    app.route('/players')
        .get(isLoggedIn, function (req, res) {
            Player.find({}, function (err, players) {
                if(err){
                    console.log('ERROR: get /players');
                    res.send(err);
                }else{
                    res.status(200);
                    res.json(players);
                }
            })
        });

    app.route('/updateTeamTrainers')
        .post(function (req, res) {
            Team.findOne({"_id": req.body.teamId}, function (err, team) {
                team.trainers = req.body.trainers;
                team.save();
                res.status(200);
                res.send();
            })
        });


    app.route('/users')
        .get(isLoggedIn, function (req, res) {
            User.find({}, function (err, users) {
                if(err){
                    console.log('ERROR: get /users');
                    res.send(err);
                }else{
                    res.json(users);
                }
            })
        });

    app.route('/createUser')
        .post(passport.authenticate('local-signup', {}));

    app.route('/updateUser')
        .post(function (req, res) {
            User.findOne({"_id": req.body.id}, function (err, user) {
                user.local.name= req.body.name;
                user.local.username = req.body.username;
                user.local.rights = req.body.rights;
                user.local.isAdmin = req.body.isAdmin;
                if(req.body.password){
                    user.local.password = user.generateHash(req.body.password)
                }
                user.save();
                res.send();
            })
        });

    app.route('/deleteUser')
        .post(function (req, res) {
            User.findOne({"_id": req.body.id}, function (err, user) {
                user.remove();
                res.send();
            })
        });



}; //Module.exports end

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

// Hjælpemetoder:
function getDate(activity) {
    var tempDate = activity.date.split("-");
    return new Date(tempDate[2] + "-" + tempDate[1] + "-" + tempDate[0]);
}

var insertActivitySorted = function (res, newActivity, team) {
    if (team.activities.length) {
        var isDone = false;
        var activDate = new Date(getDate(newActivity));
        for (var i = 0; i < team.activities.length; i++) {
            var teamDate = new Date(getDate(team.activities[i]));
            if (activDate < teamDate) {
                console.log("date check 1");
                team.activities.splice(i, 0, newActivity);
                team.save();
                isDone = true;
                res.send(team);
                return;
            } else if (activDate.getTime() == teamDate.getTime()) {
                console.log("date check 2");
                var tempActivTime = newActivity.startingTime.split(":");
                var activHr = parseInt(tempActivTime[0]);
                var activMin = parseInt(tempActivTime[1]);

                var tempTeamTime = team.activities[i].startingTime.split(":");
                var teamHr = parseInt(tempTeamTime[0]);
                var teamMin = parseInt(tempTeamTime[1]);
                if (activHr < teamHr) {
                    console.log("tidscheck 1");
                    team.activities.splice(i, 0, newActivity);
                    team.save();
                    isDone = true;
                    res.send(team);
                    return;
                } else if (activHr == teamHr) {
                    if (activMin <= teamMin) {
                        console.log("tidscheck 2");
                        team.activities.splice(i, 0, newActivity);
                        team.save();
                        isDone = true;
                        res.send(team);
                        return;
                    }
                }
            }
        }
        if (!isDone) {
            console.log("not done");

            team.activities.push(newActivity);
            team.save();
            res.send(team);
            return;
        }
    } else {
        console.log("else");

        team.activities.push(newActivity);
        team.save();
        res.send(team);
        return;
    }
};