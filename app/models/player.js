var mongoose = require('mongoose');

var playerSchema = mongoose.Schema({
    name: String,
    year: Number,
    gender: String
});
module.exports = mongoose.model('player', playerSchema);