var mongoose = require('mongoose');

var teamSchema = mongoose.Schema({
    name: String,
    players: [],
    trainers: [],
    activities: []
});
module.exports = mongoose.model('team', teamSchema);