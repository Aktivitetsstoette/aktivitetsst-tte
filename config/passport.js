// loader alle de ting vi har brug for.
var LocalStrategy = require('passport-local').Strategy;

// loader usermodellen.
var User = require('./../app/models/user');

// eksporterer funktionen vha. module.exports, så denne kan hentes i app.js
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // nødvendig for at bruge persistente sessions.
    // passport skal bruge muligheden for at serialisere og deserialisere bruger ud af en session.

    // bruges til at serialiserer brugeren i sessionen.
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // bruges til at deserialiserer brugeren i sessionen.
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // Vi bruger navngivne strategier, fordi vi har en for både login og signup.

    passport.use('local-signup', new LocalStrategy({
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // Tillader os at sende hele requestet med videre i callback'en.
        },
        function(req, username, password, done) {

            // Asynkron
            // User.findOne vil ikke blive kaldt, medmindre data data kommer tilbage.
            process.nextTick(function() {

                // Finden en bruger hvis brugernavn er det samme som det indtastede.
                // Vi tjekker om den bruger, der prøver at logge ind findes i databasen.
                User.findOne({ 'local.username' :  username.toLowerCase() }, function(err, user) {
                    // Hvis der kommer en fejl, så returneres fejlen inden andet bliver tjekket på.
                    if (err)
                        return done(err);

                    // Hvis brugernavnet indtastet findes i databasen.
                    if (user) {
                        return done(null, false);
                    } else {

                        // Hvis brugernavnet indtastet ikke findes i databasen oprettes brugeren.
                        var newUser            = new User();

                        // sætter brugerens local attributter
                        newUser.local.username = username.toLowerCase();    //alle usernames i lowercase
                        newUser.local.password = newUser.generateHash(password);
                        newUser.local.name = req.body.name;
                        newUser.local.rights = req.body.rights;
                        newUser.local.isAdmin = req.body.isAdmin;
                        newUser.local.secure = false;

                        // gemmer brugeren i databasen.
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null);
                        });
                    }
                });

            });

        }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // Vi bruger navngivne strategier, fordi vi har en for både login og signup.

    passport.use('local-login', new LocalStrategy({
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // Tillader os at sende hele requestet med videre i callback'en.
        },
        function(req, username, password, done) { // callback med brugernavn og password
            // Finden en bruger hvis brugernavn er det samme som det indtastede.
            // Vi tjekker om den bruger, der prøver at logge ind findes i databasen.
            User.findOne({ 'local.username' :  username.toLowerCase() }, function(err, user) {
                // Hvis der kommer en fejl, så returneres fejlen inden andet bliver tjekket på.
                if (err) {
                    return done(err);
                }

                // Hvis brugernavnet indtastet ikke findes i databasen.
                if (!user) {
                    return done(null, false, req.flash('loginMessage', 'Det indtastede brugernavn findes ikke.'));
                }

                // Hvis brugernavnet findes, men kodeordet er forkert.
                if (!user.validPassword(password)) {
                    return done(null, false, req.flash('loginMessage', 'Forkert kodeord.'));
                }

                // Alle tjeks er blevet godkendt, så brugeren returneres og adgang til systemet er godkendt.
                return done(null, user);
            });
        }));
};