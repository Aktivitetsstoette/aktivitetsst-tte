$(function () {
    var teams = [];
    var players = [];
    var users = [];
    var years = [];
    var getterPromises = [];

    //-------------------------------------------------------------------------
    // Funktioner der returnerer promises med POST og GET til/fra databasen
    //-------------------------------------------------------------------------

    //----------- POST team -----------------------------------
    var postNewTeamPromise = function (newTeam) {
        var promise = new Promise(function (resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/createTeam',
                data: newTeam,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    };

    function postUpdateTeamTrainersPromise(updatedTeam){
        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/updateTeamTrainers',
                data: updatedTeam,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    }

    function postUpdateTeamPlayersPromise(updatedTeam){
        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/updateTeamPlayers',
                data: updatedTeam,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    }
    //----------- POST players --------------------------------
    var postNewPlayerPromise = function (newPlayer) {
        var promise = new Promise(function (resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/createPlayer',
                data: newPlayer,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    };

    //----------- POST users ----------------------------------
    function postNewUserPromise(newUser){
        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/createUser',
                data: newUser,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    }

    function postUpdateUserPromise(newUser){
        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/updateUser',
                data: newUser,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    }

    function postDeleteUserPromise(deleteData){
        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/deleteUser',
                data: deleteData,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    }


    //----------- POST activities -----------------------------
    function postNewActivityPromise(newActivity){
        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/createActivity',
                data: newActivity,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    }

    function postUpdateActivityPromise(updateData){
        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/updateActivity',
                data: updateData,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    }

    function postDeleteActivityPromise(deleteData){
        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/deleteActivity',
                data: deleteData,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    }

    function postUpdateAttendancePromise(updateData) {
        var promise = new Promise(function(resolve, reject) {
            $.ajax({
                type: 'POST',
                url: '/updateActivity',
                data: updateData,
                complete: function () {
                    resolve();
                }
            });
        });
        return promise;
    }

    //----------- GET -----------------------------------------

    var getTeamsPromise = function () {
        var getTeams = new Promise(function (resolve, reject) {
            $.ajax({
                type: 'GET',
                url: '/teams',
                success: function (data) {
                    teams = data;
                    teams.sort(compareName);
                },
                complete: function () {
                    resolve();
                }
            })
        });
        return getTeams;
    };

    var getPlayersPromise = function () {
        var getPlayers = new Promise(function (resolve, reject) {
            $.ajax({
                type: 'GET',
                url: '/players',
                success: function (data) {
                    players=data;
                    players.sort(compareName);
                },
                complete: function () {
                    resolve();
                },
                error: function () {
                    reject();
                }
            })
        });
        return getPlayers;
    };

    var getUsersPromise = function () {
        var getUsers = new Promise(function (resolve, reject) {
            $.ajax({
                type: 'GET',
                url: '/users',
                success: function (data) {
                    users=data;
                    users.sort(compareNameUsers);
                },
                complete: function () {
                    resolve();
                }
            })
        });
        return getUsers;
    };

    //-------------------------------------------------------------------------

    //Create promises to get data form MongoDB (auto-run)
    var createGetPromises = function () {
        var getTeamsProm = getTeamsPromise();
        var getPlayersProm = getPlayersPromise();
        var getUsersProm = getUsersPromise();
        getterPromises.push(getTeamsProm, getPlayersProm, getUsersProm);
    }();

    //-------------------------------------------------------------------------

    //Create promises to render tabs
    var createTabPromises = function () {
        var tabPromises = [];

        //Fremmøde tab
        var fremmødePromise = new Promise(function (resolve, reject) {
            $.get("./templates/fremmøde.hbs", function (template) {
                var compiled = Handlebars.compile(template);
                var html = compiled({
                    title: "Fremmøde"
                });
                $('#fremmøde').append(html);
                updateTeamOptions("selAttTeam", teams);
                resolve();
            });
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var string = "" + e.target;
            var tab = string.split("#")[1];

            if(tab == "fremmøde"){
                clearAttTab();
            }
            else if(tab == "hold"){
                clearTeamTab();
            }
            else if(tab == "aktiviteter"){
                clearActTab();
            }
            else if(tab == "fremmødedokumentation"){
                clearDocTab();
            } else if(tab == "admin"){
                clearAdminTab();
            }
        });

        //Hold tab
        var holdPromise = new Promise(function (resolve, reject) {
            for (var i = (new Date).getFullYear(); i > (new Date).getFullYear() - 25; i--) {
                years.push({year: i});
            }
            $.get("./templates/hold.hbs", function (template) {
                var compiled = Handlebars.compile(template);
                var html = compiled({
                    title: "Hold",
                    years: years,
                    trainers: users
                });
                $('#hold').append(html);
                resolve();
            });
        });

        //Aktivitet tab
        var aktivitetPromise = new Promise(function (resolve, reject) {
            $.get("./templates/aktiviteter.hbs", function (template) {
                var compiled = Handlebars.compile(template);
                var html = compiled({
                    title: "Aktiviteter"
                });
                $('#aktiviteter').append(html);
                resolve();
            });
        });

        var fremmødedokPromise = new Promise(function (resolve, reject) {
            $.get("./templates/fremmødedokumentation.hbs", function (template) {
                var compiled = Handlebars.compile(template);
                 var html = compiled({
                    title: "Fremmødedokumentation"
                });
                $('#fremmødedokumentation').append(html);
                resolve();
            });
        });

        //Admin tab
        var adminPromise = new Promise(function (resolve, reject) {
            $.get("./templates/admin.hbs", function (template) {
                var compiled = Handlebars.compile(template);
                var html = compiled({
                    title: "Admin"
                });
                $('#admin').append(html);
                resolve();
            });
        });
        tabPromises.push(fremmødePromise, holdPromise, aktivitetPromise, fremmødedokPromise, adminPromise);
        return tabPromises;
    };

    //-------------------------------------------------------------------------


    //START PROMISSES
    var tabProms;
    Promise.all(getterPromises)
        .then(function () {
            tabProms = createTabPromises()
        })
        .then(function () {
            Promise.all(tabProms)
                .then(function () {
//FUNCTIONALITY
//======================================================================================================================

                    //START FREMMØDE TAB ===========================================================================

                    /**
                     * Tjek hvilken radio button der er valgt, og disable/enable de inputs der er relevante
                     */
                    var checkRadioButton = function() {
                        var aktivVal = $('input[name=attRadio]:checked').val();
                        //Antal valgt
                        if (aktivVal == "num") {
                            $('.checkOffInput').prop('disabled', true);
                            for(var i =0; i<4;i++){
                                if($('#grp'+i+'Max').text() == 0){
                                    $('#grp'+i).prop('disabled', true)
                                }else{
                                    $('#grp'+i).prop('disabled', false)
                                }
                            }
                            //Afkrydsning valgt
                        } else {
                            $('.numberInput td input').prop('disabled', true);
                            $('.checkOffInput').prop('disabled', false);
                        }
                    };

                    /**
                     * Opdatérer al data for valgt aktivitet når pop-up vinduet bliver åbnet
                     * @param title Registrer eller redigér fremmøde
                     * @param tableID id for tabel hvori der er valgt aktivitet
                     */
                    var openAttPop = function (title, tableID) {
                        $('#attendanceTitle').text(title);

                        var actIndex = $("#"+tableID+" .active").attr("value");
                        var team = getTeamById($('#selAttTeam option:selected').val());
                        var activity = team.activities[actIndex];

                        //Opdatér data om aktivitet i pop-up vindue
                        $('#attPopTeam').text(team.name);
                        $('#attPopDate').text(activity.date);
                        $('#attPopStartingTime').text(activity.startingTime);
                        $('#attPopName').text(activity.name);
                        var hr = activity.time.split(":")[0];
                        var min = activity.time.split(":")[1];
                        $('#attTimeHr').val(hr);
                        $('#attTimeMin').val(min);

                        //Hent spillere ind og tæl op i aldersgrupper
                        var thisYear = new Date().getFullYear();
                        var grupper = [0,0,0,0];
                        $('#tblCheckOff tbody tr td').empty();
                        for(var i in players){
                            if(team.players.indexOf(players[i]._id) != -1){
                                if(thisYear - players[i].year > 2 && thisYear - players[i].year <6){
                                    grupper[0]++;
                                } else if(thisYear - players[i].year < 13){
                                    grupper[1]++;
                                } else if(thisYear - players[i].year < 19){
                                    grupper[2]++;
                                } else if(thisYear - players[i].year < 24){
                                    grupper[3]++;
                                } else{
                                    //DU ER FOR GAMMEL!
                                }
                                $('#tblCheckOff tbody tr td')
                                    .append('<div class="checkbox">' +
                                        '<label>' +
                                        '<input class="checkOffInput" type="checkbox" value="'+players[i]._id+'">'+players[i].name+'</label>' +
                                        '</div>');
                            }
                        }
                        //Opdatér spans og læg max på input fields
                        for(var i in grupper){
                            $('#grp'+i+'Max').text(grupper[i]);
                            $('#grp'+i).prop('max', grupper[i]);
                            if(activity.attendance !=null){
                                $('#grp'+i).val(activity.attendance[i]);
                            } else{
                                $('#grp'+i).val(0);
                            }
                        }

                        //Vælg default radio button
                        $('#rbNumber').prop('checked', true);
                        checkRadioButton();
                        $('.checkOffInput').prop('checked', false);
                        $('.numberInput').val("");
                    };

                    //Optælling at hvor mange spillere der er registreret til at være fremmødt + aktivitetstid
                    /**
                     * Tæller hvor mange spillere der er registeret til fremmøde, og læser aktivitetstid
                     * Returner det optalte fremmøde og aktivitetstiden
                     * @returns {{attendance: [number,number,number,number], time: string}}
                     */
                    var readAttendance = function () {
                        var att = [0,0,0,0];
                        if($('#rbNumber').is(':checked')){
                            for(var i = 0; i<4;i++){
                                att[i] = $('#grp'+i).val();
                            }
                        } else {
                            var checkedPlayers =[];
                            for(var i in $('.checkOffInput')){
                                if($('.checkOffInput')[i].checked){
                                    checkedPlayers.push($('.checkOffInput')[i].value);
                                }
                            }
                            var thisYear = new Date().getFullYear();
                            for(var i in players){
                                if(checkedPlayers.indexOf(players[i]._id) != -1){
                                    if(thisYear - players[i].year >2 && thisYear - players[i].year <6){
                                        att[0]++;
                                    } else if(thisYear - players[i].year < 13){
                                        att[1]++;
                                    } else if(thisYear - players[i].year < 19){
                                        att[2]++;
                                    } else if(thisYear - players[i].year < 24){
                                        att[3]++;
                                    } else {
                                        //DU ER FOR GAMMEL TIL STØTTE
                                    }
                                }
                            }
                        }
                        var time = $('#attTimeHr').val()+":"+ $('#attTimeMin').val();
                        return {attendance: att, time: time};
                    };

                    //Change team
                    $('#selAttTeam').change(function () {
                        updateAttTables();
                    });

                    //Change radio button
                    $('input[name=attRadio]').change(function () {
                        checkRadioButton();
                    });

                    $('#registerAttendance').click(function(){
                        openAttPop("Registrér fremmøde", "tblMissAtt");
                        $('#rbCheckOff').prop('disabled', false);
                    });

                    $('#editAttendance').click(function(){
                        openAttPop("Redigér fremmøde", "tblRegAtt");
                        $('#rbCheckOff').prop('disabled', true);
                    });

                    $('#sendAtt').click(function () {
                        var att = readAttendance();
                        if(att.attendance[0] == 0 && att.attendance[1] == 0 && att.attendance[2] == 0 && att.attendance[3] == 0){
                            alert("Der er ikke registreret noget fremmøde!");
                            return;
                        }
                        if ($('#grp'+0).val() > $('#grp'+0).prop('max')) {
                            alert('Du har indtastet et antal for "3-5 år", der overskrider maksimum!');
                            return;
                        } else if ($('#grp'+1).val() > $('#grp'+1).prop('max')) {
                            alert('Du har indtastet et antal for "6-12 år", der overskrider maksimum!');
                            return;
                        } else if ($('#grp'+2).val() > $('#grp'+2).prop('max')) {
                            alert('Du har indtastet et antal for "13-18 år", der overskrider maksimum!');
                            return;
                        } else if ($('#grp'+3).val() > $('#grp'+3).prop('max')) {
                            alert('Du har indtastet et antal for "19-24 år", der overskrider maksimum!');
                            return;
                        }

                        var actIndex;
                        if($("#tblMissAtt .active").attr("value") != undefined){
                            actIndex = $("#tblMissAtt .active").attr("value")
                        }else{
                            actIndex = $("#tblRegAtt .active").attr("value")
                        }

                        var team = getTeamById($('#selAttTeam option:selected').val());
                        var activity = team.activities[actIndex];
                        var updateData ={
                            teamId: team._id,
                            searchParam1: activity.date,
                            searchParam2: activity.startingTime,
                            name: activity.name,
                            date: null,
                            startingTime: activity.startingTime,
                            time: att.time,
                            attendance: att.attendance
                        };

                        var updateAtt = postUpdateAttendancePromise(updateData);
                        updateAtt.then(function () {
                            var getTeamsProm = getTeamsPromise();
                            getTeamsProm.then(function () {
                                $('#attPopWindow').modal('toggle');
                                updateAttTables();
                            })
                        })
                    });

                    //SLUT FREMMØDE TAB ============================================================================


                    // START HOLD TAB ==============================================================================

                    $('#btnCreateTeam').click(function () {
                        $('#popTeamName').val("");
                        $('.popTeamTrainers').prop('checked', false);
                    });

                    //Create team
                    $('#createTeam').click(function () {
                        if ($('#popTeamName').val().trim().length == 0) {
                            alert("Du har ikke indtastet et holdnavn!");
                            return;
                        }

                        event.preventDefault();
                        var trainersArray = $('.popTeamTrainers');
                        var pickedTrainers = [];
                        for (var i = 0; i < trainersArray.length; i++) {
                            if (trainersArray[i].checked) {
                                pickedTrainers.push(trainersArray[i].value)
                                $('.popTeamTrainers')[i].checked = false;
                            }
                        }
                        var newTeam = {
                            name: $('#popTeamName').val().trim(),
                            trainers: pickedTrainers,
                            teams: teams

                        };

                        var postPromise = postNewTeamPromise(newTeam);
                        postPromise.then(function () {
                            var getTeamsProm = getTeamsPromise();
                            getTeamsProm.then(
                                function () {
                                    updateTeamOptions("selHold",teams);
                                    $('#opretHold').modal('hide');
                                    clearTeamTab();
                                })
                        });
                    });

                    $('#btnCreatePlayer').click(function () {
                        $('#popPlayerName').val("");
//                        $('#popPlayerYear option:first').prop('selected', true);
//                        $('#popPlayerGender option:first').prop('selected', true);
                    });

                    //Create player
                    $('#createPlayer').click(function () {
                        if ($('#popPlayerName').val().trim().length == 0) {
                            alert("Du har ikke indtastet et spillernavn!");
                            return;
                        }

                        event.preventDefault();
                        var newPlayer = {
                            name: $('#popPlayerName').val().trim(),
                            year: $('#popPlayerYear').val(),
                            gender: $('#popPlayerGender').val()
                        };

                        var postPromise = postNewPlayerPromise(newPlayer);
                        postPromise.then(function () {
                            var getPlayersProm = getPlayersPromise();
                            getPlayersProm.then(
                                function () {
                                    updateTeamInfo();
                                    $('#opretSpiller').modal('hide');
                                }
                            )
                        });
                    });

                    $('#selHold').change(function () {
                        updateTeamInfo();
                    });

                    $('#selGender, #selYear').change(function () {
                        updateTempPlayers();
                    });

                    $('#addPlayers').click(function () {
                        addPlayers("otherPlayers","teamPlayers")
                    });

                    $('#removePlayers').click(function () {
                        removePlayers("teamPlayers","otherPlayers")
                    });

                    $("#savePlayerChanges").click(function () {
                        var playerIds = [];
                        for (var i = 0; i < teamPlayers.length; i++) {
                            playerIds.push(teamPlayers[i]._id)
                        }
                        var updatedTeam = {
                            teamId: $('#selHold option:selected').val(),
                            players: playerIds,
                            teamName: $('#teamName').val().trim()
                        };

                        var updateTeamPlayersProm = postUpdateTeamPlayersPromise(updatedTeam);
                        updateTeamPlayersProm.then(function () {
                            var getTeamsProm = getTeamsPromise();
                            getTeamsProm.then( function () {
                                clearTeamTab();
                            })
                        })
                    });

                    $('#forgetPlayerChanges').click(function () {
                        updateTeamInfo();
                    });

                    $('#addTrainers').click(function () {
                        addTrainer("otherTrainersPop", "teamTrainersPop")
                    });

                    $('#removeTrainers').click(function () {
                        removeTrainer("teamTrainersPop", "otherTrainersPop")
                    });

                    $("#saveTrainerChanges").click(function () {
                        var trainerIds = [];
                        for (var i = 0; i < teamTrainers.length; i++) {
                            trainerIds.push(teamTrainers[i]._id)
                        }
                        var updatedTeam = {
                            teamId: $('#selHold option:selected').val(),
                            trainers: trainerIds
                        };

                        var updateTeamTrainersProm = postUpdateTeamTrainersPromise(updatedTeam);
                        updateTeamTrainersProm.then(function () {
                            var getTeamsProm = getTeamsPromise();
                            getTeamsProm.then( function () {
                                updateTeamInfo();
                                $('#redigerTrænere').modal('hide');

                            })
                        })
                    });

                    $('#forgetTrainerChanges').click(function () {
                        updateTeamInfo();
                    });

                    //SLUT HOLD TAB ================================================================================


                    //START AKTIVITET TAB ==========================================================================

                    $('#actStartDate, #actEndDate').change(function () {
                        updateActTable();
                    });

                    $('#selActTeam').change(function () {
                        $('#btnCreateActivity').prop("disabled",false);
                        $('#btnClearActivity').prop("disabled",false);
                        clearActInputs();
                        updateActTable();
                    });

                    $('#btnClearActivity').click(function () {
                        clearActInputs();
                    });

                    //Create activity
                    $('#btnCreateActivity').click(function(){
                        if(checkActivityFields()) {
                            var newActivity = {
                                name: $('#activityName').val().trim(),
                                date: new Date($('#activityDate').val()),
                                startingTime: $('#activityStartHr').val() + ":" + $('#activityStartMin').val(),
                                time: $('#activityTimeHr').val() + ":" + $('#activityTimeMin').val(),
                                teamId: $('#selActTeam').val()
                            };


                            var postActivityProm = postNewActivityPromise(newActivity);
                            postActivityProm.then(function () {
                                var getTeamsProm = getTeamsPromise();
                                getTeamsProm
                                    .then(function () {
                                        updateActTable();
                                        clearActInputs();
                                    })
                            })
                        }
                    });

                    //Save changes
                    $('#btnSaveActivity').click(function () {
                        if(checkActivityFields()) {
                            var updateData = {
                                teamId: $('#selActTeam option:selected').val(),
                                searchParam1: $("#tblActivity .active").children()[1].textContent,
                                searchParam2: $("#tblActivity .active").children()[2].textContent,
                                name: $('#activityName').val().trim(),
                                date: new Date($('#activityDate').val()),
                                startingTime: $('#activityStartHr').val() + ":" + $('#activityStartMin').val(),
                                time: $('#activityTimeHr').val() + ":" + $('#activityTimeMin').val()
                            };

                            var updateActProm = postUpdateActivityPromise(updateData);
                            updateActProm.then(function () {
                                var getTeamsProm = getTeamsPromise();
                                getTeamsProm
                                    .then(function () {
                                        updateActTable();
                                        clearActInputs();
                                    })
                            });
                        }
                    });

                    //Delete activity
                    $('#btnDeleteActivity').click(function () {
                        if (confirm("Er du sikker på du vil slette aktiviteten?")) {
                            var deleteData = {
                                teamId: $('#selActTeam option:selected').val(),
                                searchParam1: $("#tblActivity .active").children()[1].textContent,
                                searchParam2: $("#tblActivity .active").children()[2].textContent
                            };

                            var deleteActProm = postDeleteActivityPromise(deleteData);
                            deleteActProm.then(function () {
                                var getTeamsProm = getTeamsPromise();
                                getTeamsProm
                                    .then(function () {
                                        updateActTable();
                                        clearActInputs();
                                    })
                            });
                        }
                    });

                    //SLUT AKTIVITET TAB ===========================================================================


                    //START FREMMØDEDOK TAB ====================================================================================

                    $('#sel-doc').change(function(){
                        var selectedDoc = $('#sel-doc option:selected');
                        if(selectedDoc.val() == "team"){
                            $('#sel-team').prop('disabled', false);
                            $('#table-doc').empty();
                            $('#btnPrint').prop("disabled", true);
                            $('#btnExport').prop("disabled", true);
                            $('#docStartDate').prop("disabled", true);
                            $('#docEndDate').prop("disabled", true);
                            //show fremmøde pr hold
                        }
                        else{
                            $('#sel-team')[0].selectedIndex = 0;
                            $('#sel-team').prop('disabled', true);
                            //show samletOversigt
                            $('#table-doc').empty();
                            loadAllDoc();
                            $('#btnPrint').prop("disabled", false);
                            $('#btnExport').prop("disabled", false);
                            $('#docStartDate').prop("disabled", false);
                            $('#docEndDate').prop("disabled", false);
                        }
                    });

                    $('#sel-team').change(function(){
                        loadTeamDoc();
                        $('#btnPrint').prop("disabled", false);
                        $('#btnExport').prop("disabled", false);
                        $('#docStartDate').prop("disabled", false);
                        $('#docEndDate').prop("disabled", false);
                    });

                    $('#docEndDate, #docStartDate').change(function(){
                        var selectedDoc = $('#sel-doc option:selected');
                        if(selectedDoc.val() == "team"){
                            loadTeamDoc();
                        }else if (selectedDoc.val() == "all"){
                            loadAllDoc();
                        }
                    });

                    $('#btnPrint').click(function() {
                        $('.printable').printThis();
                    });

                    $('#btnExport').click(function(){
                        if($('#sel-doc option:selected').val() == "team"){
                            var filename = $('#sel-team option:selected').attr('data-otherValue');
                        }else{
                            var filename = "Samlet";
                        }

                        $("#table-doc").table2excel({
                            // exclude CSS class
                            exclude: ".noExl",
                            name: "Worksheet Name",
                            filename: "Fremmøde-"+filename //do not include extension
                        });
                    });

                    //SLUT FREMMØDEDOK TAB =========================================================================


                    //START ADMIN TAB ==============================================================================

                    //Create user
                    $('#btnCreateUser').click(function () {
                        if($('#adminName').val().trim().length == 0 || $('#adminUsername').val().trim().length == 0 ||
                            $('#adminPassword').val().trim().length == 0){
                            alert("Alle felter skal udfyldes for at oprette en bruger");
                            return;
                        }
                        var isAdmin = false;
                        if ($('input[name=rights]:checked').val() == 'Admin') {
                            isAdmin = true;
                        }
                        var newUser = {
                            name: $('#adminName').val().trim(),
                            username: $('#adminUsername').val().trim(),
                            password: $('#adminPassword').val().trim(),
                            rights: $('input[name=rights]:checked').val(),
                            isAdmin: isAdmin
                        };
                        
                        var postUserProm = postNewUserPromise(newUser);
                        postUserProm.then(function () {
                            var getUsersProm = getUsersPromise();
                            getUsersProm.then(function () {
                                adminClearFields();
                                updateUserTable();
                            });
                        })
                    });

                    //Save user
                    $('#btnSaveUser').click(function () {
                        var isAdmin = false;
                        if ($('input[name=rights]:checked').val() == 'Admin') {
                            isAdmin = true;
                        }
                        var newUser = {
                            id: $('#tblUsers .active').attr('value'),
                            name: $('#adminName').val().trim(),
                            username: $('#adminUsername').val().trim(),
                            password: $('#adminPassword').val().trim(),
                            rights: $('input[name=rights]:checked').val(),
                            isAdmin: isAdmin
                        };

                        var updateUserProm = postUpdateUserPromise(newUser);
                        updateUserProm.then(function () {
                            var getUsersProm = getUsersPromise();
                            getUsersProm.then(function () {
                                adminClearFields();
                                updateUserTable();
                            });
                        })
                    });

                    //Clear fields
                    $('#btnAdminClear').click(function () {
                        $('#tblUsers .active').removeClass('active');
                        adminClearFields();
                    });

                    //Delete user
                    $('#btnDeleteUser').click(function () {
                        var user = getUserById($('#tblUsers .active').attr('value'));
                        if(user.local.secure){
                            alert("Du kan ikke slette denne bruger!")
                            return;
                        }
                        if (confirm("Er du sikker på du vil slette brugeren?")) {
                            var deleteData = {
                                id: $('#tblUsers .active').attr('value')
                            };

                            var deleteUserProm = postDeleteUserPromise(deleteData);
                            deleteUserProm.then(function () {
                                var getUsersProm = getUsersPromise();
                                getUsersProm
                                    .then(function () {
                                        updateUserTable();
                                        adminClearFields();
                                    })
                            });
                        }
                        $('#btnDeleteUser').prop('disabled', true);
                    });

                    //SLUT ADMIN TAB ===============================================================================

                });
        });

    //-------------------------------------------------------------------------
    // Generelle hjælpemetoder
    //-------------------------------------------------------------------------

    /**
     * Returnerer et hold med id der matcher id parameteret
     * @param teamID id der søges efter
     * @returns {*}
     */
    function getTeamById(teamID){
        for(var i in teams){
            if(teams[i]._id == teamID){
                return teams[i];
            }
        }
    }

    /**
     * Returnerer en bruger med id der matcher id parameteret
     * @param userID id der søges efter
     * @returns {*}
     */
    function getUserById(userID) {
        for(var i in users){
            if(users[i]._id == userID){
                return users[i];
            }
        }
    }

    function updateTeamOptions(dataId, data) {
        $('#' + dataId).empty();
        var rows = '<option disabled selected value>--Vælg hold--</option>';
        if($('#userLoggedIn').attr('otherValue') == "true") {
            $.each(data, function (index, item) {
                var row = '<option value="' + item._id + '" data-otherValue="' + item.name + '">' + item.name + '</option>';
                rows += row;
            });
        }
        else{
            var userId= $('#userLoggedIn').attr('value');
            for(var i =0; i<teams.length;i++){

                for(var j=0; j<teams[i].trainers.length;j++){
                    if(teams[i].trainers[j]==userId){
                        var row = '<option value="' + teams[i]._id + '" data-otherValue="' + teams[i].name + '">' + teams[i].name + '</option>';
                        rows += row;
                    }
                }
            }
            $('#btnCreateTeam').hide();
            $('#editTrainers').hide();
        }
        $('#' + dataId).html(rows);
    }

    // Slut generelle metoder
    //-------------------------------------------------------------------------
    // Hjælpemetoder til Fremmødetab
    //-------------------------------------------------------------------------
    function updateAttTables(){
        var team = getTeamById($('#selAttTeam option:selected').val());

        //empty and insert new data into tables
        $('#tblMissAtt').empty();
        $('#tblRegAtt').empty();
        $('#editAttendance').prop("disabled", true);
        $('#registerAttendance').prop("disabled", true);
        var index =0; //bliver lagt som value på table rows, for at afspejle index i holds aktivitets array
        for(var i in team.activities){
            var newDate= team.activities[i].date.split("-");
            var checkDate= new Date(newDate[2] + "-" + newDate[1] + "-" + newDate[0]);

            if(team.activities[i].attendance == null && checkDate<new Date()){
                $('#tblMissAtt')
                    .append('<tr value="'+index+'">' +
                        '<td>'+team.activities[i].date+'</td>' +
                        '<td>'+team.activities[i].startingTime+'</td>' +
                        '<td>'+team.activities[i].name+'</td>' +
                        '</tr>');
                index++;
            } else if (team.activities[i].attendance != null) {
                $('#tblRegAtt')
                    .append('<tr value="'+index+'">' +
                        '<td>'+team.activities[i].date+'</td>' +
                        '<td>'+team.activities[i].startingTime+'</td>' +
                        '<td>'+team.activities[i].name+'</td>' +
                        '</tr>');
                index++;
            }
        }

        $('#tblMissAtt, #tblRegAtt').find('tr').click(function(){
            $("#tblMissAtt .active, #tblRegAtt .active").removeClass('active');
            var activity = $(this);
            activity.addClass('active');
            if(activity.closest('tbody').attr('id') == "tblMissAtt") {
                $('#registerAttendance').prop('disabled', false);
                $('#editAttendance').prop('disabled', true);
            } else {
                $('#editAttendance').prop('disabled', false);
                $('#registerAttendance').prop('disabled', true);
            }
        });
    }

    function clearAttTab() {
        updateTeamOptions("selAttTeam", teams);

        $('#tblMissAtt').empty();
        $('#tblRegAtt').empty();
        $('#registerAttendance').prop('disabled', true);
        $('#editAttendance').prop('disabled', true);
    }

    // Slut Fremmødetab
    //-------------------------------------------------------------------------
    // Hjælpemetoder til Holdtab
    //-------------------------------------------------------------------------
    // Team lister
    var teamPlayers = [];
    var otherPlayers = [];
    var teamTrainers = [];
    var otherTrainers = [];

    function addPlayers(dataIdFrom, dataIdTo) {
        var newFromPlayers = [];
        var newToPlayers = teamPlayers;

        for (var i = 0; i < $('#' + dataIdFrom + ' div label input').length; i++) {
            if ($('#' + dataIdFrom + ' div label input')[i].checked) {
                newToPlayers.push(otherPlayers[i])
            } else {
                newFromPlayers.push(otherPlayers[i])
            }
        }
        teamPlayers = newToPlayers;
        otherPlayers = newFromPlayers;
        loadCheckboxPlayers(dataIdFrom,otherPlayers);
        loadCheckboxPlayers(dataIdTo,teamPlayers);
    }

    function removePlayers(dataIdFrom, dataIdTo) {
        var newFromPlayers = [];
        var newToPlayers = otherPlayers;

        for (var i = 0; i < $('#' + dataIdFrom + ' div label input').length; i++) {
            if ($('#' + dataIdFrom + ' div label input')[i].checked) {
                newToPlayers.push(teamPlayers[i])
            } else {
                newFromPlayers.push(teamPlayers[i])

            }
        }
        teamPlayers = newFromPlayers;
        otherPlayers = newToPlayers;
        loadCheckboxPlayers(dataIdFrom,teamPlayers);
        loadCheckboxPlayers(dataIdTo,otherPlayers);
    }

    function addTrainer(dataIdFrom, dataIdTo) {
        var newFromTrainers = [];
        var newToTrainers = teamTrainers;

        for (var i = 0; i < $('#' + dataIdFrom + ' div label input').length; i++) {
            if ($('#' + dataIdFrom + ' div label input')[i].checked) {
                newToTrainers.push(otherTrainers[i])
            } else {
                newFromTrainers.push(otherTrainers[i])
            }
        }
        teamTrainers = newToTrainers;
        otherTrainers = newFromTrainers;
        loadCheckboxTrainers(dataIdTo,teamTrainers);
        loadCheckboxTrainers(dataIdFrom,otherTrainers);
    }

    function removeTrainer(dataIdFrom, dataIdTo) {
        var newFromTrainers = [];
        var newToTrainers = otherTrainers;

        for (var i = 0; i < $('#' + dataIdFrom + ' div label input').length; i++) {
            if ($('#' + dataIdFrom + ' div label input')[i].checked) {
                newToTrainers.push(teamTrainers[i])
            } else {
                newFromTrainers.push(teamTrainers[i])
            }
        }
        teamTrainers = newFromTrainers;
        otherTrainers = newToTrainers;
        loadCheckboxTrainers(dataIdFrom,teamTrainers);
        loadCheckboxTrainers(dataIdTo,otherTrainers);
    }

    function loadCheckboxPlayers(dataId, data) {
        $('#' + dataId).empty();
        var rows = '';
        $.each(data, function(index, item) {
            rows += '<div class="checkbox"> <label><input type="checkbox" value='+ item._id +'>'+item.name+'</label> </div>'
        });
        $('#' + dataId).html(rows);
    }

    function loadCheckboxTrainers(dataId, data) {
        $('#' + dataId).empty();
        var rows = '';
        $.each(data, function(index, item) {
            if (!item.local.isAdmin) {
                rows += '<div class="checkbox"> <label><input class="popTeamTrainers" type="checkbox" value='+ item._id +'>'+item.local.name+'</label> </div>'
            }
        });
        $('#' + dataId).html(rows);
    }

    function loadTrainers(dataId, data) {
        $('#' + dataId).empty();
        var rows = '';
        $.each(data, function(index, item) {
            if (!item.local.isAdmin) {
                rows += item.local.name+'</br>';
            }

        });
        $('#' + dataId).html(rows);
    }

    function clearTeamTab() {
        updateTeamOptions("selHold", teams);
        loadCheckboxTrainers("createTeamTrainers",users);
        teamPlayers = [];
        otherPlayers = [];
        $('#teamPlayers').empty();
        $('#otherPlayers').empty();
        $('#teamTrainersList').empty();
        $('#teamName').val("");
        $('#selGender')[0].selectedIndex==0;
        $('#selYear')[0].selectedIndex==0;
        $('#removePlayers').prop("disabled",true);
        $('#addPlayers').prop("disabled",true);
        $('#selYear').prop("disabled",true);
        $('#selGender').prop("disabled",true);
        $('#editTrainers').prop("disabled",true);
        $('#savePlayerChanges').prop("disabled",true);
        $('#forgetPlayerChanges').prop("disabled",true);
    }

    var updateTeamInfo = function () {
        var teamId = $('#selHold').val();
        if(teamId){
            $('#removePlayers').prop("disabled",false);
            $('#addPlayers').prop("disabled",false);
            $('#selYear').prop("disabled",false);
            $('#selGender').prop("disabled",false);
            $('#editTrainers').prop("disabled",false);
            $('#savePlayerChanges').prop("disabled",false);
            $('#forgetPlayerChanges').prop("disabled",false);

            //opdatere holdnavn
            $('#teamName').val($('#selHold').find('option:selected').attr('data-otherValue'));

            //opdatere holdspillere
            var team = getTeamById(teamId);

            var teamPlayerIds = team.players;
            teamPlayers = [];
            otherPlayers = [];
            players.forEach(function (item) {
                if(teamPlayerIds.indexOf(item._id)>-1){
                    teamPlayers.push(item);
                }
                else{
                    var gender = $('#selGender').val();
                    if(gender=='all' || gender == item.gender){
                        var year = $('#selYear').val();
                        if(year=='all' || year == item.year ){
                            otherPlayers.push(item);
                        }
                    }
                }
            });

            var teamTrainerIds = team.trainers;
            teamTrainers = [];
            otherTrainers = [];
            users.forEach(function (item) {
                if (!item.local.isAdmin) {
                    if(teamTrainerIds.indexOf(item._id)>-1){
                        teamTrainers.push(item);
                    } else{
                        otherTrainers.push(item);
                    }
                }
            });

            //opdatere team spillere
            loadCheckboxPlayers("teamPlayers", teamPlayers);
            //opdatere andre spillere
            loadCheckboxPlayers("otherPlayers",otherPlayers);
            //opdatere team trænere
            loadTrainers("teamTrainersList",teamTrainers);
            loadCheckboxTrainers("teamTrainersPop",teamTrainers);
            //opdatere andre trænere
            loadCheckboxTrainers("createTeamTrainers",users);
            loadCheckboxTrainers("otherTrainersPop",otherTrainers);
        }
    };

    var updateTempPlayers = function() {
        var teamId = $('#selHold').val();
        if (teamId) {
            //opdatere holdspillere
            var teamPlayerIds = [];
            for(var i in teamPlayers){
                teamPlayerIds.push(teamPlayers[i]._id)
            }
            otherPlayers = [];
            players.forEach(function (item) {
                if(teamPlayerIds.indexOf(item._id) === -1){
                    var gender = $('#selGender').val();
                    if(gender=='all' || gender == item.gender){
                        var year = $('#selYear').val();
                        if(year=='all' || year == item.year ){
                            otherPlayers.push(item);
                        }
                    }
                }
            });
            loadCheckboxPlayers("otherPlayers", otherPlayers);
        }
    };

    // Slut Holdtab
    //-------------------------------------------------------------------------
    // Hjælpemetoder til Aktivitetertab
    //-------------------------------------------------------------------------
    function clearActTab(){
        updateTeamOptions("selActTeam", teams);

        $('#tblActivity').empty();
        $('#teamLabel').text("");
        clearActInputs();
        $('#btnCreateActivity').prop("disabled",true);
        $('#btnDeleteActivity').prop("disabled", true);
        $('#btnClearActivity').prop("disabled", true);
        var date = new Date();
        var month = date.getMonth();
        var year = date.getFullYear();
        if (month+2 > 12) {
            month = "01";
            year++;
        }
        $('#actStartDate').val(date.getFullYear() + "-" + (date.getMonth()+1) + "-01");
        $('#actEndDate').val(year + "-" + month +"-01");
    }

    function clearActInputs() {
        $('#activityName').val("");
        $('#activityDate').val("");
        $('#activityStartHr').val("12");
        $('#activityStartMin').val("00");
        $('#activityTimeHr').val("0");
        $('#activityTimeMin').val("0");
        $("#tblActivity .active").removeClass('active');
        $('#btnCreateActivity').prop('disabled',false);
        $('#btnSaveActivity').prop('disabled',true);
        $('#btnDeleteActivity').prop("disabled", true);
    }

    var updateActTable = function() {
        if ($('#selActTeam').find('option:selected').val()){
            $('#teamLabel').text($('#selActTeam').find('option:selected').attr('data-otherValue'));
            $('#tblActivity').children().remove();
            var teamID = $('#selActTeam').val();
            var team = getTeamById(teamID);
            for (var i in team.activities) {
                var activity = team.activities[i];
                var activDate = getDate(activity);
                var startDate = new Date($('#actStartDate').val());
                var endDate = new Date($('#actEndDate').val());

                if (activDate >= startDate && activDate <= endDate) {
                    $('#tblActivity').append("<tr>" +
                        "<td>" + activity.name + "</td>" +
                        "<td>" + getDanishDateString(activity) + "</td>" +
                        "<td>" + getPeriod(activity) + "</td>" +
                        "<td>" + getTimeString(activity) + "</td>" +
                        "</tr>")
                }
            }
            clickActivity();
        }
    };

    //Click on activity in table
    var clickActivity = function () {
        $('#tblActivity').find('tr').click(function(){
            $("#tblActivity .active").removeClass('active');
            $('#btnSaveActivity').prop('disabled', false);
            $('#btnCreateActivity').prop('disabled', true);
            $('#btnDeleteActivity').prop("disabled", false);
            var activity = $(this);
            activity.addClass('active');
            var children = activity.children();
            $('#activityName').val(children[0].textContent);
            var dateInfo = children[1].textContent.split('-');
            $('#activityDate').val(dateInfo[2] + "-" + dateInfo[1] + "-" + dateInfo[0]);
            var startHr = children[2].textContent.split(":")[0];
            var startMin = children[2].textContent.split("-")[0].split(":")[1];
            $('#activityStartHr').val(startHr);
            $('#activityStartMin').val(startMin);
            var timeHr = children[3].textContent.split(":")[0];
            var timeMin = children[3].textContent.split(":")[1];
            $('#activityTimeHr').val(timeHr);
            $('#activityTimeMin').val(timeMin);
        });
    };

    // Slut Aktivitetstab
    //-------------------------------------------------------------------------
    // Hjælpemetoder til Fremmødedoktab
    //-------------------------------------------------------------------------

    // indlæser fremmøde pr hold
    function loadTeamDoc() {
        var selected = $('#sel-team option:selected');
        var dataRows = [];
        $('#table-doc').load('templates/teamdoc.html',function () {
            var team;
            if (new Date($('#docStartDate').val()) <= new Date($('#docEndDate').val())) {
                for(var i in teams){
                    if(teams[i]._id == selected.val()){
                        team=teams[i];
                        var teamActivities= teams[i].activities;
                        for (var a in teamActivities) {
                            var tempDate = teamActivities[a].date.split("-");
                            var actDate = new Date(tempDate[2] + "-" + tempDate[1] + "-" + tempDate[0]);
                            if(teamActivities[a].attendance && actDate >= new Date($('#docStartDate').val()) && actDate <= new Date($('#docEndDate').val())){
                                var activity = teamActivities[a];
                                var activityTime = getTime(activity);
                                var dataRow = {
                                    name: activity.name,
                                    date: getDanishDateString(activity),
                                    period: getPeriod(activity),
                                    time: getTimeString(activity),
                                    count1: activity.attendance[0],
                                    count2: activity.attendance[1],
                                    count3: activity.attendance[2],
                                    count4: activity.attendance[3],
                                    hours1: (activity.attendance[0]*activityTime).toFixed(2),
                                    hours2: (activity.attendance[1]*activityTime).toFixed(2),
                                    hours3: (activity.attendance[2]*activityTime).toFixed(2),
                                    hours4: (activity.attendance[3]*activityTime).toFixed(2)
                                };
                                dataRows.push(dataRow);
                            }
                        }
                    }
                }

                if (dataRows.length != 0) {
                    loadTeamTable('doc-data',['name','date','period','time','count1','count2','count3','count4','hours1','hours2','hours3','hours4'], dataRows);
                    insertDateString();
                    $('#showTeamName').text("Hold: " + team.name);
                } else {
                    showEmptyTable('doc-data', 12, team.name +" har ingen fremmøderegistreringer for den givne periode.");
                }
            } else {
                showEmptyTable('doc-data', 12, "Den valgte periode er ugyldig.");
            }
        });
    }

    function insertDateString(){
        var tempStartDate = new Date($('#docStartDate').val());
        var startDate = tempStartDate.getDate() + "-" + (tempStartDate.getMonth()+1) + "-" + tempStartDate.getFullYear();
        var tempEndDate = new Date($('#docEndDate').val());
        var endDate = tempEndDate.getDate() + "-" + (tempEndDate.getMonth()+1) + "-" + tempEndDate.getFullYear();
        $('#period').text("Dokumentationen gælder for perioden fra " + startDate + " til " + endDate);
    }

    // indlæser samlet oversigt
    function loadAllDoc() {
        $('#table-doc').load('templates/alldoc.html',function () {
            var dataRows = [];
            var totalSumOfAttendaceHours1 = 0;
            var totalSumOfAttendaceHours2 = 0;
            var totalSumOfAttendaceHours3 = 0;
            var totalSumOfAttendaceHours4 = 0;

            if (new Date($('#docStartDate').val()) <= new Date($('#docEndDate').val())) {
                for(var i in teams){
                    var teamActivities= teams[i].activities;
                    var sumOfActivityTime = 0;
                    var activityCount = 0;
                    var sumAttendance1 = 0;
                    var sumAttendance2 = 0;
                    var sumAttendance3 = 0;
                    var sumAttendance4 = 0;

                    var sumOfAttendanceHours1 = 0;
                    var sumOfAttendanceHours2 = 0;
                    var sumOfAttendanceHours3 = 0;
                    var sumOfAttendanceHours4 = 0;

                    var avgAttendance1 = 0;
                    var avgAttendance2 = 0;
                    var avgAttendance3 = 0;
                    var avgAttendance4 = 0;

                    for (var a in teamActivities) {
                        var tempDate = teamActivities[a].date.split("-");
                        var actDate = new Date(tempDate[2] + "-" + tempDate[1] + "-" + tempDate[0]);
                        if(teamActivities[a].attendance && actDate >= new Date($('#docStartDate').val()) && actDate <= new Date($('#docEndDate').val())){
                            activityCount++;
                            var activity = teamActivities[a];
                            var activityTime = getTime(activity);
                            sumOfActivityTime += activityTime;
                            sumAttendance1 += parseInt(teamActivities[a].attendance[0]);
                            sumAttendance2 += parseInt(teamActivities[a].attendance[1]);
                            sumAttendance3 += parseInt(teamActivities[a].attendance[2]);
                            sumAttendance4 += parseInt(teamActivities[a].attendance[3]);
                            sumOfAttendanceHours1 += parseInt(teamActivities[a].attendance[0])* activityTime;
                            sumOfAttendanceHours2 += parseInt(teamActivities[a].attendance[1])* activityTime;
                            sumOfAttendanceHours3 += parseInt(teamActivities[a].attendance[2])* activityTime;
                            sumOfAttendanceHours4 += parseInt(teamActivities[a].attendance[3])* activityTime;
                        }
                    }
                    totalSumOfAttendaceHours1 += sumOfAttendanceHours1;
                    totalSumOfAttendaceHours2 += sumOfAttendanceHours2;
                    totalSumOfAttendaceHours3 += sumOfAttendanceHours3;
                    totalSumOfAttendaceHours4 += sumOfAttendanceHours4;

                    if(activityCount > 0){
                        var avgAttendance1 = sumAttendance1/activityCount;
                        var avgAttendance2 = sumAttendance2/activityCount;
                        var avgAttendance3 = sumAttendance3/activityCount;
                        var avgAttendance4 = sumAttendance4/activityCount;
                    }
                    var dataRow = {
                        team: teams[i].name,
                        actPlace: "",
                        weekday: "",
                        start: "",
                        end:"",
                        sumOfActivityTime: sumOfActivityTime.toFixed(2),
                        avgAttendance1: avgAttendance1.toFixed(2),
                        avgAttendance2: avgAttendance2.toFixed(2),
                        avgAttendance3: avgAttendance3.toFixed(2),
                        avgAttendance4: avgAttendance4.toFixed(2),
                        sumOfAttendaceHours1: sumOfAttendanceHours1.toFixed(2),
                        sumOfAttendaceHours2: sumOfAttendanceHours2.toFixed(2),
                        sumOfAttendaceHours3: sumOfAttendanceHours3.toFixed(2),
                        sumOfAttendaceHours4: sumOfAttendanceHours4.toFixed(2)
                    };
                    dataRows.push(dataRow);
                }
                if (dataRows.length != 0) {
                    loadTeamTable('doc-data',['team','actPlace','weekday','start','end','sumOfActivityTime','avgAttendance1','avgAttendance2','avgAttendance3','avgAttendance4','sumOfAttendaceHours1','sumOfAttendaceHours2','sumOfAttendaceHours3','sumOfAttendaceHours4'],dataRows);
                    $('#total1').text(totalSumOfAttendaceHours1.toFixed(2));
                    $('#total2').text(totalSumOfAttendaceHours2.toFixed(2));
                    $('#total3').text(totalSumOfAttendaceHours3.toFixed(2));
                    $('#total4').text(totalSumOfAttendaceHours4.toFixed(2));
                    insertDateString();
                } else {
                    showEmptyTable('doc-data', 14, "Ingen fremmøderegistreringer for den givne periode.");
                }
            }else {
                showEmptyTable('doc-data', 14, "Den valgte periode er ugyldig.");
            }
            $('#docStartDate').prop(disabled, false);
            $('#docEndDate').prop(disabled, false);
        });
    }

    function loadTeamTable(tableId, fields, data) {
        var rows = '';
        $.each(data, function(index, item) {
            rows += '<tr>';
            var count = 0;
            $.each(fields, function(index, field) {
                count++;
                if(count<4){
                    rows += '<td>' + item[field+''] + '</td>';
                }
                else{
                    rows += '<td style="text-align:center">' + item[field+''] + '</td>';
                }
            });
            rows += '<tr>';
        });
        $('#doc-data').html(rows);
    }

    function showEmptyTable(tableId, columnSpan, message) {
        $('#' + tableId).empty();
        $('#'+tableId).append('<tr><td colspan="' + columnSpan+ '" style="text-align: center">' + message + '</td><tr>');
    }

    function clearDocTab() {
        updateTeamOptions("sel-team", teams);

        $('#table-doc').empty();

        $('#sel-doc').empty();
        var rows = '<option disabled selected value><i>--Vælg dokument--</i></option>';
        rows += '<option value="team">Fremmøde pr. hold</option>';
        rows += '<option value="all">Samlet oversigt</option>';
        $('#sel-doc').html(rows);

        var date = new Date();
        var day = date.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        $('#docStartDate').val(date.getFullYear() + "-" + (date.getMonth()) + "-01");
        $('#docEndDate').val(date.getFullYear() + "-" + (date.getMonth()+1) +"-"+ day);
        $('#docStartDate').prop("disabled", true);
        $('#docEndDate').prop("disabled", true);
        $('#btnPrint').prop("disabled", true);
        $('#btnExport').prop("disabled", true);
        $('#sel-team').prop("disabled", true);

    }

    // Slut Doctab
    //-------------------------------------------------------------------------
    // Hjælpemetoder til Admintab
    //-------------------------------------------------------------------------

    function clearAdminTab(){
        updateUserTable();
        adminClearFields();
    }

    /**
     * Opdatérer bruger tabellen
     */
    function updateUserTable() {
        $('#tblUsers').empty();
        for(var i in users){
            $('#tblUsers')
                .append('<tr value="'+users[i]._id+'">' +
                    '<td>' + users[i].local.name+ '</td>' +
                    '<td>' + users[i].local.username+ '</td>' +
                    '<td>' + users[i].local.rights+ '</td>' +
                    '</tr>');
        }
        clickOnUserTbl();
    };

    /**
     * Klik på bruger tabel
     */
    function clickOnUserTbl() {
        $('#tblUsers').find('tr').click(function () {
            $('#btnDeleteUser').prop('disabled', false);
            $('#btnCreateUser').prop('disabled', true);
            $('#btnSaveUser').prop('disabled', false);
            $("#tblUsers .active").removeClass('active');
            var user = $(this);
            user.addClass('active');
            var children = user.children();
            $('#adminName').val(children[0].textContent);
            $('#adminUsername').val(children[1].textContent);
            $('#adminPassword').prop('placeholder', 'Evt. nyt kodeord');
            if (children[2].textContent == "Admin") {
                $('#rbAdmin').prop('checked', true);
            } else {
                $('#rbTrainer').prop('checked', true);
            }
        });
    };

    /**
     * Rydder alle felter i admin oprettelse/ændring
     */
    function adminClearFields() {
        $('#adminName').val("");
        $('#adminUsername').val("");
        $('#adminPassword').val("");
        $('#adminPassword').removeAttr('placeholder');
        $('#rbTrainer').prop('checked', true);
        $('#btnCreateUser').prop('disabled', false);
        $('#btnSaveUser').prop('disabled', true);
        $('#btnDeleteUser').prop('disabled', true);
    };

    // Slut Admintab
    //-------------------------------------------------------------------------
    // Hjælpemetoder til activity (Bruges i fremmøde-tab og activity-tab)
    //-------------------------------------------------------------------------

    function getTime(activity){
        var t2 = parseInt(activity.time.split(":")[0]);
        var m2 = parseInt(activity.time.split(":")[1]);
        return (t2 + (m2/60));
    }

    function getTimeString(activity) {
        var t2 = parseInt(activity.time.split(":")[0]);
        var m2 = parseInt(activity.time.split(":")[1]);
        if(m2<10){
            m2 = "0"+m2;
        }
        return t2 + ":"+ m2;
    }

    function getDate(activity) {
        var tempDate = activity.date.split("-");
        return new Date(tempDate[2] + "-" + tempDate[1] + "-" + tempDate[0]);
    }

    function getDanishDateString(activity) {
        var tempDate = activity.date.split("-");
        return tempDate[0] + "-" + tempDate[1] + "-" + tempDate[2];
    }

    function getPeriod(activity) {
        var t1 = parseInt(activity.startingTime.split(":")[0]);
        var m1 = parseInt(activity.startingTime.split(":")[1]);
        var t2 = parseInt(activity.time.split(":")[0]);
        var m2 = parseInt(activity.time.split(":")[1]);
        var endHour = Math.floor((t1 + t2 + ((m1+m2)/60)) % 24);
        if(endHour < 10){
            endHour = "0" + endHour;
        }
        var endMinute = (m1+m2)%60;
        if(endMinute < 10){
            endMinute = "0" + endMinute;
        }
        return activity.startingTime + "-" + endHour + ":" + endMinute
    }

    function compareName(a,b) {
        if (a.name.toLowerCase() <= b.name.toLowerCase()) {
            return -1;
        } else {
            return 1;
        }
    }

    function compareNameUsers(a,b) {
        if (a.local.name.toLowerCase() <= b.local.name.toLowerCase()) {
            return -1;
        } else {
            return 1;
        }
    }

    function checkActivityFields(){
        if(!$('#selActTeam').val()){
            alert("Du skal huske at vælge hold!");
            return false;
        }
        if ($('#activityName').val().trim().length == 0) {
            alert("Du skal huske at skrive et navn for aktiviteten!");
            return false;
        }
        if ($('#activityDate').val().length == 0) {
            alert("Du skal huske at vælge en gyldig dato!");
            return false;
        }
        if ($('#activityTimeHr').val() == 0 && $('#activityTimeMin').val() == 0) {
            alert("Du skal huske at indtaste hvor lang tid aktiviteten varer!");
            return false;
        }
        if($('#activityStartHr').val().length > 2 || $('#activityStartMin').val().length > 2){
            alert("Du skal vælge et gyldigt starttidspunkt");
            return false;
        }
        if($('#activityStartHr').val().length == 0 || $('#activityStartMin').val().length == 0){
            alert("Du skal vælge et gyldigt starttidspunkt");
            return false;
        }
        if(parseInt($('#activityStartHr').val()) > 23 || parseInt($('#activityStartMin').val()) > 59){
            alert("Du skal vælge et gyldigt starttidspunkt");
            return false;
        }
        return true;
    }
});
