"use strict";
var express  = require('express');
var app      = express();
var mongoose = require('mongoose');
var passport = require('passport');
var request = require('request');
var flash    = require('connect-flash');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

var db = require('./config/database.js');

mongoose.connect(db.url);   // connect mongoose

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('public'));

app.set('view engine', 'hbs');
app.set('views', __dirname + '/public/templates');

app.set('port', (process.env.PORT || 8080)); // Set the port

// required for passport
app.use(session({secret: 'aktivitetstoetteapp'})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash());

require('./config/passport')(passport); // pass passport for configuration

// ROUTES FOR OUR APP
// =============================================================================
require('./app/routes.js')(app, passport);

// START THE SERVER
// =============================================================================

app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));
});

